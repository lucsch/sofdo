from sqlalchemy import *
from migrate import *

meta = MetaData()

sofdo_downloads = Table('sofdo_downloads', meta,
                        Column('id', Integer, primary_key=True),
                        Column('softversion', String(10)),
                        Column('softname', String(65)),
                        Column('softextension', String(10)),
                        Column('usermail', String(255)),
                        Column('userupdate',Integer, default = 1),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    meta.bind = migrate_engine
    sofdo_downloads.create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    meta.bind = migrate_engine
    sofdo_downloads.drop()
