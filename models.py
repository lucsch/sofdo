import datetime as datetime
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Download(Base):
    __tablename__ = 'sofdo_downloads'

    id = Column(Integer, primary_key=True)
    softversion = Column(String(10))
    softname = Column(String(65))
    softextension = Column(String(10))
    usermail = Column(String(255))
    userupdate = Column(Integer, default=1)
    userplateform = Column(String(60))
    userbrowser = Column(String(60))
    userlanguage = Column(String(20))
    userstring = Column(String(255))
    userdatetime = Column(DateTime)
    userip = Column(String(20))
    usercountry = Column(String(30))
    userregion = Column(String(40))
    usercity = Column(String(60))
    userlatlong = Column(String(100))
    usertimezone = Column(String(100))

    def __init__(
            self,
            softversion=None,
            softname=None,
            softextension=None,
            usermail=None,
            userupdate=1,
            userplateform=None,
            userbrowser=None,
            userlanguage=None,
            userstring=None,
            userip=None,
            usercountry=None,
            userregion=None,
            usercity=None,
            userlatlong=None,
            usertimezone=None,
            userdatetime=datetime.datetime.now(),

    ):
        self.softversion = softversion
        self.softname = softname
        self.softextension = softextension
        self.usermail = usermail
        self.userupdate = userupdate
        self.userplateform = userplateform
        self.userbrowser = userbrowser
        self.userlanguage = userlanguage
        self.userstring = userstring
        self.userip = userip
        self.usercountry = usercountry
        self.userregion = userregion
        self.usercity = usercity
        self.userlatlong = userlatlong
        self.usertimezone = usertimezone
        self.userdatetime = userdatetime

    def __repr__(self):
        return "<id {},  softname {}>".format(self.id, self.softname)