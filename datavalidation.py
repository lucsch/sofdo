#!/usr/bin/python
import re

##########################################################
# DATA VALIDATION, check data validity and small data
# manipulation
# Fri Dec 30 00:34:01 2016
# (c) Lucien SCHREIBER
##########################################################


def ValidateHasCorrectArgs(args):
    if 'email' in args:
        return True

    return False


def ValidateEmail(email):
    if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return False

    return True


def ValidateIsExternalIp(ip):
    if re.match(r"(^127\.)|(^10\.)|(^172\.1[6-9]\.)|(^172\.2[0-9]\.)|(^172\.3[0-1]\.)|(^192\.168\.)", ip):
        return False

    return True