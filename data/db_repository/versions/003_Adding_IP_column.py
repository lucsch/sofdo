from sqlalchemy import *
from migrate import *


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    meta = MetaData(bind=migrate_engine)
    sofdo_downloads = Table('sofdo_downloads', meta, autoload=True)
    Column('userip', String(20)).create(sofdo_downloads)


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    meta = MetaData(bind=migrate_engine)
    sofdo_downloads = Table('sofdo_downloads', meta, autoload=True)
    sofdo_downloads.c.userip.drop()
