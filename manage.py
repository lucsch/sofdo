#!/usr/bin/env python
from migrate.versioning.shell import main
from config import SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO

if __name__ == '__main__':
    main(debug='False', url=SQLALCHEMY_DATABASE_URI, repository=SQLALCHEMY_MIGRATE_REPO)
