# Sofdo
Software dowloading

## Installation

create git repository
`$ git init`

virtualenv
`pyvenv-3.5 env`
`source env/bin/activate`

## Running code (locally)

`python app.py`

or Using *GUNICORN*

`gunicorn app:app`

## Deploying code to heroku

Create an heroku application before pushing code with `heroku create`

1. Commit your code
2. Push your code to the heroku `git push heroku master`

## Creating Postgres database on Heroku

`heroku addons:add heroku-postgresql`
hero


## Deploying database update on Heroku

The first time, use the command `heroku run init` in order to create the
migration table into the database. Then each time you want to migrate
your database, use the command `heroku run update`

Other command be added into the Procfile file if needed.

## Ressources

https://realpython.com/blog/python/flask-by-example-part-1-project-setup/
https://devcenter.heroku.com/articles/getting-started-with-python#deploy-the-app
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xii-facelift

### GeoIP

https://freegeoip.net/?q=62.167.126.56