import os
basedir = os.path.abspath(os.path.dirname(__file__))

# default value for developpement (local database)
SQLALCHEMY_DATABASE_URI = "postgresql://localhost/sofdo"

# production database adress and credentials are
# coded in this URL
if('DATABASE_URL') in os.environ:
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')

SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, "data", 'db_repository')
