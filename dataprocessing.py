#!/usr/bin/python
import os
import urllib
from models import Download
from datavalidation import ValidateIsExternalIp

##########################################################
# DATA PROCESING, read file data from text file and
# generate code for html template
# Wed Dec 28 17:44:43 2016
# (c) Lucien SCHREIBER
##########################################################

class SoftLink(object):
    """docstring for SoftLink"""
    def __init__(self):
        self.linkVersion = ""
        self.linkName = ""
        self.linkExtension = ""
        self.linkSize = ""
        self.linkDate = ""
        self.linkAdress = ""
        self.HasError = False

    def GetDataFromText(self, line):
        items = line.split(";")
        if (len(items) != 6):
            self.HasError = True
            pass

        self.linkName = items[0]
        self.linkVersion = items[1]
        self.linkExtension = items[2]
        self.linkSize = items[3]
        self.linkDate = items[4]
        self.linkAdress = items[5]


class SoftFile(object):
    """docstring for SoftFile"""
    def __init__(self, filename):
        self.fileName = filename
        self.softName = ""
        self.softDesc = ""
        self.softLinks = []

    def ComputeInfo(self):
        with open(self.fileName) as txtfile:
            for index, line in enumerate(txtfile.readlines()):
                if index == 0:
                    self.softName = line[:-1]
                    continue
                if index == 1:
                    self.softDesc = line[:-1]
                    continue
                else:
                    # process software version and download links
                    myLink = SoftLink()
                    myLink.GetDataFromText(line)
                    self.softLinks.append(myLink)


class SoftAll(object):
    """docstring for SoftAll"""
    def __init__(self):
        self.path = ""
        pass

    def GetAllSoft(self, path='data/project'):
        mySofts = []
        self.path = path
        for file in os.listdir(self.path):
            if file.endswith(".txt"):
                software = SoftFile(os.path.join(self.path, file))
                software.ComputeInfo()
                mySofts.append(software)
        return mySofts

    def GetSoftPath(self):
        return self.path


class GeoIPInfo(object):
    """Compute GeoIP based"""
    def __init__(self, ip):
        self.ip = ip
        self.isExternalIp = True
        self.ipdata = None
        self.status = ""

        # verify ip address for external ip, excluding localhost, etc.
        if ValidateIsExternalIp(self.ip) is False:
            self.isExternalIp = False
            self.status = "localhost"
        else:
            # get geoinformation from http://freegeoip.net
            try:
                response = urllib.request.urlopen("http://freegeoip.net/csv/{}".format(self.ip))
                self.ipdata = response.read().decode("utf-8").split(",")
                print (self.ipdata)
            except:
                self.status = "error"


    def __VerifyIpData(self):
        if self.ipdata is None:
            return False

        if self.status == False:
            return  False

        # freegeoip should return 11 entry
        if len(self.ipdata) != 11:
            self.status = "error"
            return False

        return True


    def GetCountry(self):
        if self.__VerifyIpData() == False:
            return self.status
        return self.ipdata[2]


    def GetRegion(self):
        if self.__VerifyIpData() == False:
            return None
        return self.ipdata[4]


    def GetCity(self):
        if self.__VerifyIpData() == False:
            return None
        return self.ipdata[5]


    def GetLatLong(self):
        if self.__VerifyIpData() == False:
            return None
        return self.ipdata[8] + "/" + self.ipdata[9]


    def GetTimezone(self):
        if self.__VerifyIpData() == False:
            return None
        return self.ipdata[7]

    def GetIp(self):
        return self.ip


class SoftDataBase(object):
    """Class for Database manipulation"""
    def __init__(self, databasehandle):
        self.database = databasehandle

    def InsertDownload(self, request):
        requestargs = request.args
        keepmeposted = 0
        if 'keepmeposted' in requestargs:
            keepmeposted = 1

        gip = GeoIPInfo(request.remote_addr)

        self.database.session.add(
            Download(
                requestargs['linkVersion'],
                requestargs['softname'],
                requestargs['linkExtension'],
                requestargs['email'],
                keepmeposted,
                request.user_agent.platform,
                request.user_agent.browser,
                request.accept_languages[0][0],
                request.user_agent.string,
                gip.GetIp(),
                gip.GetCountry(),
                gip.GetRegion(),
                gip.GetCity(),
                gip.GetLatLong(),
                gip.GetTimezone()
            ))
        self.database.session.commit()






