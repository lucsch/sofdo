from sqlalchemy import *
from migrate import *


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    meta = MetaData(bind=migrate_engine)
    sofdo_downloads = Table('sofdo_downloads', meta, autoload=True)
    Column('userplateform', String(60)).create(sofdo_downloads)
    Column('userbrowser', String(60)).create(sofdo_downloads)
    Column('userlanguage', String(20)).create(sofdo_downloads)
    Column('userstring', String(255)).create(sofdo_downloads)
    Column('userdatetime', DateTime).create(sofdo_downloads)


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    meta = MetaData(bind=migrate_engine)
    sofdo_downloads = Table('sofdo_downloads', meta, autoload=True)
    sofdo_downloads.c.userplateform.drop()
    sofdo_downloads.c.userbrowser.drop()
    sofdo_downloads.c.userlanguage.drop()
    sofdo_downloads.c.userstring.drop()
    sofdo_downloads.c.userdatetime.drop()
