from sqlalchemy import *
from migrate import *


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    meta = MetaData(bind=migrate_engine)
    sofdo_downloads = Table('sofdo_downloads', meta, autoload=True)
    Column('usercountry', String(30)).create(sofdo_downloads)
    Column('userregion', String(40)).create(sofdo_downloads)
    Column('usercity', String(60)).create(sofdo_downloads)
    Column('userlatlong', String(100)).create(sofdo_downloads)
    Column('usertimezone', String(100)).create(sofdo_downloads)


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    meta = MetaData(bind=migrate_engine)
    sofdo_downloads = Table('sofdo_downloads', meta, autoload=True)
    sofdo_downloads.c.usercountry.drop()
    sofdo_downloads.c.userregion.drop()
    sofdo_downloads.c.usercity.drop()
    sofdo_downloads.c.userlatlong.drop()
    sofdo_downloads.c.usertimezone.drop()
