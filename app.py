from flask import Flask
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
from flask_sqlalchemy import SQLAlchemy

from dataprocessing import SoftAll
from dataprocessing import SoftDataBase

from datavalidation import ValidateEmail
from datavalidation import ValidateHasCorrectArgs
from werkzeug.contrib.fixers import ProxyFix

app = Flask(__name__)
app.config.from_object('config')
app.secret_key = 'thisisasecretkey'
app_name = "Sofdo"


db = SQLAlchemy(app)
app.wsgi_app = ProxyFix(app.wsgi_app)


@app.route('/')
def index():
    return render_template('home.html', project_name=app_name)

@app.route('/download')
def download():
    mysoft = SoftAll()
    return render_template(
        'download.html',
        project_name=app_name,
        page_name='download',
        data=mysoft.GetAllSoft())


@app.route('/download_form', methods=['GET', 'POST'])
def download_form():
    if ValidateHasCorrectArgs(request.args):
        if ValidateEmail(request.args['email']):
            # insert info into database and launch download
            sdb = SoftDataBase(db)
            sdb.InsertDownload(request)

            # goto success page.
            return redirect(url_for(
                'download_sucess',
                project_name=app_name,
                page_name='download',
                softname=request.args['softname'],
                linkVersion=request.args['linkVersion'],
                linkAdress=request.args['linkAdress']))
        else:
            flash("Please specify a valid email adress!")

    return render_template(
        'download_form.html',
        project_name=app_name,
        page_name='download',
        data=request.args)


@app.route('/download_sucess', methods=['GET', 'POST'])
def download_sucess():
    # process linkAdress to add frame code
    linkAdress = request.args['linkAdress']
    linkPreview = linkAdress.replace('/s/', '/embed/preview/')
    return render_template(
        'download_sucess.html',
        project_name=app_name,
        page_name='download',
        linkAdressPreview=linkPreview,
        data=request.args)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('notfound.html'), 404


if __name__ == '__main__':
    app.run(debug=True)
